package nab.api.com.au;

import nab.api.com.au.Service.ProductsService;
import nab.api.com.au.entity.Category;
import nab.api.com.au.entity.Products;
import nab.api.com.au.repository.productsJpaRepository;
import nab.api.com.au.repository.categoryJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ContextConfiguration(classes = { PersistenceTestConfig.class }, loader = AnnotationConfigContextLoader.class)
@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@DataJpaTest
public class ProductsServiceTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private ProductsService productsService;

    @Autowired
    private productsJpaRepository productsRepo;

    @Autowired
    private categoryJpaRepository categoryRepo;

    @Test
    public void create_new_product() {
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Products products = new Products();
        products.setName("TestName");
        products.setDescription("Test Desc");
        products.setCategory(category);
        productsRepo.save(products);
        productsRepo.flush();

        Optional<Products> actualProduct = productsService.findById(products.getId());
        actualProduct.ifPresent(pro -> {
            assertEquals(pro.getName(), products.getName());
        });
    }

    @Test
    public void productServiceTest()
    {
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Products products = new Products();
        products.setName("TestName");
        products.setDescription("Test Desc");
        products.setCategory(category);
        productsService.addProduct(products);

        Products actualProduct = productsService.get(products.getId());

        assertEquals(actualProduct.getName(), products.getName());
    }

    @Test
    public void invalidProductTest()
    {
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Products products = new Products();
        products.setName("TestName");
        products.setDescription("Test Desc");
        products.setCategory(category);
        productsService.addProduct(products);

        Products actualProduct = productsService.get(100L);

        assertEquals(actualProduct, null);

    }

    @Test
    public void getProductsByCategoryTest(){
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Products products = new Products();
        products.setName("TestName");
        products.setDescription("Test Desc");
        products.setCategory(category);
        productsService.addProduct(products);

        List<Products> actualProduct = productsService.getProductsByCategory(category);

        assertEquals(actualProduct.get(0).getName(), products.getName());
    }

    @Test
    public void deleteProductTest(){
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Products products = new Products();
        products.setName("TestName");
        products.setDescription("Test Desc");
        products.setCategory(category);
        productsService.addProduct(products);

        productsService.deleteProduct(productsService.get(products.getId()));

        Products actualProduct = productsService.get(products.getId());

        assertNull(actualProduct);
    }

    @Test
    public void editProduct(){
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Products products = new Products();
        products.setName("TestName");
        products.setDescription("Test Desc");
        products.setCategory(category);
        productsService.addProduct(products);

        final Products changedProduct = productsService.get(products.getId());
        changedProduct.setName("Name Changed");
        productsService.updateProduct(changedProduct);

        Products actualProduct = productsService.get(products.getId());

        assertEquals(changedProduct.getName(), actualProduct.getName());
    }

}
