package nab.api.com.au;

import nab.api.com.au.Service.CategoryService;
import nab.api.com.au.Service.ProductsService;
import nab.api.com.au.entity.Category;
import nab.api.com.au.repository.productsJpaRepository;
import nab.api.com.au.repository.categoryJpaRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@ContextConfiguration(classes = { PersistenceTestConfig.class }, loader = AnnotationConfigContextLoader.class)
@RunWith(SpringRunner.class)
@AutoConfigurationPackage
@DataJpaTest
public class CategoryServiceTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private categoryJpaRepository categoryRepo;

    @Test
    public void categoryServiceTest()
    {
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        Category actualCategory = categoryService.get(category.getId());

        assertEquals(actualCategory.getName(), category.getName());
    }

    @Test
    public void getAllTest()
    {
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        final Category category2 = new Category();
        category2.setName("Test Cat");
        categoryRepo.save(category2);
        categoryRepo.flush();

        List<Category> actualCategory = categoryService.getAll();

        assertEquals(actualCategory.get(0).getName(), category.getName());
        assertEquals(actualCategory.get(1).getName(), category2.getName());

    }

    @Test
    public void categoryNotFoundTest()
    {
        final Category category = new Category();
        category.setName("Test Cat");
        categoryRepo.save(category);
        categoryRepo.flush();

        Category actualCategory = categoryService.get(100L);

        assertNull(actualCategory);
    }
}
