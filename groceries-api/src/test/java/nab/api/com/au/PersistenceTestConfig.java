package nab.api.com.au;

import nab.api.com.au.Service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersistenceTestConfig {

    @Bean
    public ProductsService productsService() {
        return new ProductsService();
    }

    @Bean
    public CategoryService categoryService() {
        return new CategoryService();
    }
}
