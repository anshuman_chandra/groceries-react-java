package nab.api.com.au.repository;

import nab.api.com.au.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface categoryJpaRepository extends JpaRepository<Category, Long> {

}
