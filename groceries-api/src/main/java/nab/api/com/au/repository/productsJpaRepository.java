package nab.api.com.au.repository;

import nab.api.com.au.entity.Category;
import nab.api.com.au.entity.Products;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface productsJpaRepository extends JpaRepository<Products, Long> {
    List<Products> findByCategory(Category category);
}
