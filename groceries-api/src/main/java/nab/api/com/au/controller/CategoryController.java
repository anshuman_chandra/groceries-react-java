package nab.api.com.au.controller;

import nab.api.com.au.Service.CategoryService;
import nab.api.com.au.entity.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/category")
@CrossOrigin(origins = "*", maxAge = 360000)
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<Category> getCategoryProducts(@PathVariable Long id) {
        Category category =  categoryService.get(id);
        if(category != null)
        {
            return new ResponseEntity<>(category, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping
    public ResponseEntity<List<Category>> list() {
        List<Category> category = categoryService.getAll();
        if(category != null){
            return new ResponseEntity<>(category, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
