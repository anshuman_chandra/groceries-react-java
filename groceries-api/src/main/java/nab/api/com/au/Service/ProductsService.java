package nab.api.com.au.Service;

import nab.api.com.au.entity.Category;
import nab.api.com.au.entity.Products;
import nab.api.com.au.repository.productsJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductsService {

    @Autowired
    productsJpaRepository productsRepo;

    @Autowired
    CategoryService categoryService;

    public Products get(Long id) {
        return productsRepo.findById(id).orElse(null);
    }

    public Optional<Products> findById(Long id) {
        return productsRepo.findById(id);
    }

    public List<Products> getProductsByCategory(Category category)
    {
        return productsRepo.findByCategory(category);
    }

    public List<Products> getAllProducts()
    {
        return  productsRepo.findAll();
    }

    public void addProduct(Products product)
    {
        productsRepo.save(product);
    }

    public void deleteProduct(Products product)
    {
        productsRepo.delete(product);
    }

    public void updateProduct(Products product)
    {
        productsRepo.save(product);
    }

}
