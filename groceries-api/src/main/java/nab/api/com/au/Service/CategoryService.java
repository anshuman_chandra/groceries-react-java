package nab.api.com.au.Service;

import nab.api.com.au.entity.Category;
import nab.api.com.au.repository.categoryJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private categoryJpaRepository categoryRepo;

    public Category get(Long id) {
        return categoryRepo.findById(id).orElse(null);
    }

    public List<Category> getAll()
    {
        return categoryRepo.findAll();
    }
}
