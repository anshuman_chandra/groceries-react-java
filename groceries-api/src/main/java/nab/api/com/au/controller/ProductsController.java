package nab.api.com.au.controller;

import nab.api.com.au.Exception.NoProductsFoundUnderCategoryException;
import nab.api.com.au.Exception.ProductNotFoundException;
import nab.api.com.au.Service.CategoryService;
import nab.api.com.au.Service.ProductsService;
import nab.api.com.au.entity.Category;
import nab.api.com.au.entity.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "*", maxAge = 360000)
@RestController
@RequestMapping("/products")
public class ProductsController {

    private ProductsService productsService;

    private CategoryService categoryService;

    @Autowired
    public ProductsController(ProductsService productsService, CategoryService categoryService){
        this.productsService = productsService;
        this.categoryService = categoryService;
    }

    @RequestMapping
    public ResponseEntity<List<Products>> list() {
        List<Products> products = this.productsService.getAllProducts();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<Products>> allProducts(){
        List<Products> products = this.productsService.getAllProducts();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    @GetMapping(value = "/category/{categoryID}")
    public ResponseEntity<List<Products>> getProductsListByCategory(@PathVariable("categoryID") Long categoryID)
    {
        Category category = this.categoryService.get(categoryID);
        if(category == null){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        try{
            List<Products> products = this.productsService.getProductsByCategory(category);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Products> getProductByID(@PathVariable Long id){
        Products product =  this.productsService.findById(id).orElse(null);
        if(product != null) {
            return new ResponseEntity<>(product, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping()
    public ResponseEntity processAddNewProductForm(@RequestBody @Valid Products productToBeAdded, BindingResult result) {
        if(result.hasErrors()) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String[] supressedFields = result.getSuppressedFields();
        if (supressedFields.length > 0) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Category category = this.categoryService.get(productToBeAdded.getCategory().getId());

        if(category == null){
            throw new NoProductsFoundUnderCategoryException();
        }

        this.productsService.addProduct(productToBeAdded);
        return list();

    }

    @DeleteMapping("/{id}")
    public ResponseEntity processDeleteProduct(@PathVariable Long id)
    {
        Products product =  this.productsService.findById(id).orElse(null);
        if(product == null){
            throw new ProductNotFoundException();
        }
        this.productsService.deleteProduct(product);
        return list();
    }

    @PostMapping("/update/{id}")
    public ResponseEntity updateProduct(@PathVariable Long id,  @RequestBody @Valid Products productToBeUpdated)
    {
        Products product = this.productsService.get(id);
        if(product == null){
            throw new ProductNotFoundException();
        }
        this.productsService.updateProduct(productToBeUpdated);
        return list();
    }
}
