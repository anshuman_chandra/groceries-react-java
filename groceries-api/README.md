# Java Application

Clone both 
1. groceries-api (Java Application)
2. groceries-frontend (React Application)

Cd into groceries-frontend and npm install and npm run 

This will get the application up and running. 

2. groceries-api
mvn clean install (Build Application)
mvn spring-boot:run 

Create below Database and tables in MYSQL 

remember to update Host, Username and password in application.properties file. 

CREATE DATABASE `groceries`;

--
-- Database: `groceries`
--
CREATE DATABASE IF NOT EXISTS `groceries` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `groceries`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Frozen'),
(2, 'Fruits'),
(3, 'Vegetables'),
(4, 'Bakery'),
(5, 'Deli');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `price` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `category_id`) VALUES
(21, 'Tomatoes', 'Tomatoes Vegetables', 3, 3),
(22, 'Ice-cream', 'Icream frozen', 4, 1),
(23, 'Bread', 'Bread Bakery', 1, 4),
(24, 'Fish', 'Fish Deli', 5, 5),
(25, 'Mangoes', 'Mangoes Fruits', 9, 2),
(26, 'Apples', 'Apples Fruits', 3, 3);
