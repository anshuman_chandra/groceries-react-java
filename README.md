# Groceries Application Using Java and React

Clone both 
1. groceries-api (Java Application)
2. groceries-frontend (React Application)

Cd into groceries-frontend 

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!


2. groceries-api
mvn clean install (Build Application)
mvn spring-boot:run 

Create below Database and tables in MYSQL 

remember to update Host, Username and password in application.properties file. 

CREATE DATABASE `groceries`;

--
-- Database: `groceries`
--
CREATE DATABASE IF NOT EXISTS `groceries` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `groceries`;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Frozen'),
(2, 'Fruits'),
(3, 'Vegetables'),
(4, 'Bakery'),
(5, 'Deli');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(40) NOT NULL,
  `price` int(5) NOT NULL,
  `category_id` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `category_id`) VALUES
(21, 'Tomatoes', 'Tomatoes Vegetables', 3, 3),
(22, 'Ice-cream', 'Icream frozen', 4, 1),
(23, 'Bread', 'Bread Bakery', 1, 4),
(24, 'Fish', 'Fish Deli', 5, 5),
(25, 'Mangoes', 'Mangoes Fruits', 9, 2),
(26, 'Apples', 'Apples Fruits', 3, 3);
