import React, { Component } from 'react'
import { Container} from 'reactstrap'
import { NavBar } from './shared/NavBar';
import {
  BrowserRouter as Router,
  Route // for later
} from 'react-router-dom'
import HomeView from "./views/Home";
import CategoriesView from "./views/Categories";
import ProductsView from "./views/Products";


class App extends Component {


  render() {
    return (
        <Container className="App">
          <Router>
            <div>
              <NavBar />
              <Route exact path="/" component={HomeView} />
              <Route path="/categories" component={CategoriesView} />
              <Route path="/products/category/" component={(props) => <ProductsView {...props}/>}  />
          </div>
        </Router>
  </Container>
    )
  }
}

export default App