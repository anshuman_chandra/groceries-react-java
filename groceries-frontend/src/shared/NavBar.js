import React, { } from 'react';
import { Link } from "react-router-dom";

const Sep = () => <span> | </span>;

export const NavBar = () => {
    return (
        <div>
            <Link to="/">Home</Link> <Sep />
            <Link to="/categories">Categories</Link> <Sep />
            <Link to="/products/category/">Category Products</Link> <Sep />
            <hr/>
        </div>
    )
}