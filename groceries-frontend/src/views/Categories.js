import React, { Component } from 'react'
import { Link } from "react-router-dom";

class CategoriesView extends Component{
    state = {
        categories: []
    }

    getItems(){
        fetch('http://localhost:8080/category/')
            .then(response => response.json())
            .then(categories => this.setState({categories}))
            .catch(err => console.log(err))
    }

    componentDidMount(){
        this.getItems()
    }

    render() {
        return (
            <div>
                <h3>Categories</h3>
                { this.state.categories.map(({id, name}, i) =>
                    <Link key={i} to={{
                        pathname: '/products/category/',
                        id:  id
                    }}>{name}<br/></Link> ) }
            </div>
        )
    }

}

export default CategoriesView;