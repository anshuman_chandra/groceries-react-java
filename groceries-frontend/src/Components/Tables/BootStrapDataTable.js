import React , { Component } from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import ModalForm from '../Modals/Modal'
import {Button} from "reactstrap";


function showDescription(cell, row) {
    return cell.name;
}

class BootStrapDataTable extends Component {
    constructor(props) {
        super(props)
        this.deleteItem = this.deleteItem.bind(this)
    }
    deleteItem = id => {
        let confirmDelete = window.confirm('Delete item forever?')
        if(confirmDelete){
            fetch('http://localhost:8080/products/'+id, {
                method: 'delete',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    id
                })
            })
                .then(response => response.json())
                .then(item => {
                    this.props.deleteItemFromState(id)
                })
                .catch(err => console.log(err))
        }

    }

    imgFormatter = (cell, row) => {
        return <div style={{width:"110px"}}>
            <ModalForm buttonLabel="Edit" item={row} updateState={this.props.updateState} />
            {' '}
            <Button color="danger" onClick={() => this.deleteItem(row.id)}>Del</Button>
        </div>

    }
    render() {
        return (
            <div>
                <BootstrapTable data={ this.props.items }>
                    <TableHeaderColumn dataField='id' isKey={ true }>Product ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='name'>Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='description'>Description</TableHeaderColumn>
                    <TableHeaderColumn dataField='price'>Price</TableHeaderColumn>
                    <TableHeaderColumn ref='nameCol' dataField='category' dataFormat={showDescription}>Category</TableHeaderColumn>
                    <TableHeaderColumn dataFormat = {this.imgFormatter}>Action</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }
}

export default BootStrapDataTable