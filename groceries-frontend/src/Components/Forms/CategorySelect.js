import React from 'react';
import { FormGroup, Label } from 'reactstrap';

class CategorySelect extends React.Component {

    state = {
        category: [],
        selectedCat: "",
        validationError: ""
    }

    componentDidMount() {
        fetch("http://localhost:8080/category/")
            .then((response) => {
                return response.json();
            })
            .then(data => {
                let teamsFromApi = data.map(team => { return {value: team.id, display: team.name} })
                this.setState({ category: [{value: '', display: '(Select)'}].concat(teamsFromApi) });
            }).catch(error => {
            console.log(error);
        });
    }
    render() {
        return (
            <FormGroup>
                <Label for="description">Product Category</Label>
                <select className="form-control" value={this.state.selectedCat}
                        onChange={(e) => this.setState({selectedCat: e.target.value, validationError: e.target.value === "" ? "You must select your Category" : ""})}>
                    {this.state.category.map((team,i) => <option key={i} value={team.value}>{team.display}</option>)}
                </select>
                <div style={{color: 'red', marginTop: '5px'}}>
                    {this.state.validationError}
                </div>
            </FormGroup>
        )
    }
}

export default CategorySelect