import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import {toNumber} from "reactstrap/es/utils";

class AddEditForm extends React.Component {
    state = {
        id: 0,
        name: '',
        description: '',
        price: '',
        category_id: '',
        category_name : '',
        category : '',
        category_arr: []
    }

    onChange = e => {
        this.setState({[e.target.name]: e.target.value})
    }

    submitFormAdd = e => {
        let category_val = this.state.category_id.split(",");
        e.preventDefault()
        fetch('http://localhost:8080/products/', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: this.state.name,
                description: this.state.description,
                price: toNumber(this.state.price),
                category: {
                    id : category_val[0],
                    name: category_val[1]
                }
            })
        })
            .then(response => response.json())
            .then(
                item => {
                if(Array.isArray(item)) {
                    this.props.addItemToState(item[item.length -1])
                    this.props.toggle()
                } else {
                    console.log('failure')
                }
            })
            .catch(err => console.log(err))
    }

    submitFormEdit = e => {
        let category_val = this.state.category_id.split(",");
        e.preventDefault()
        fetch('http://localhost:8080/products/update/'+this.state.id, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: this.state.id,
                name: this.state.name,
                description: this.state.description,
                price: toNumber(this.state.price),
                category: {
                    id : category_val[0],
                    name: category_val[1]
                }
            })
        })
            .then(response => response.json())
            .then(item => {
                if(Array.isArray(item)) {
                    // console.log(item[0])
                    this.props.updateState(item[0])
                    this.props.toggle()
                } else {
                    console.log('failure')
                }
            })
            .catch(err => console.log(err))
    }

    componentDidMount(){
        // if item exists, populate the state with proper data
        if(this.props.item){
            this.setState({
                id : this.props.item.id,
                name: this.props.item.name,
                description: this.props.item.description,
                price: this.props.item.price,
                category_id: this.props.item.category.id+","+this.props.item.category.name,
                category_name: this.props.item.category.name
            })
        }

        fetch("http://localhost:8080/category/")
            .then((response) => {
                return response.json();
            })
            .then(data => {
                let teamsFromApi = data.map(team => { return {value: team.id+","+team.name, display: team.name} })
                this.setState({ category_arr: [{value: '', display: '(Select)'}].concat(teamsFromApi) });
            }).catch(error => {
            console.log(error);
        });
    }

    render() {
        return (
            <Form onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}>
                <FormGroup>
                    <Label for="name">Product Name</Label>
                    <Input type="text" name="name" id="name" onChange={this.onChange} value={this.state.name === null ? '' : this.state.name} required />
                </FormGroup>
                <FormGroup>
                    <Label for="description">Product Description</Label>
                    <Input type="text" name="description" id="description" onChange={this.onChange} value={this.state.description === null ? '' : this.state.description} required  />
                </FormGroup>
                <FormGroup>
                    <Label for="price">Price</Label>
                    <Input type="number" name="price" id="price" onChange={this.onChange} value={this.state.price === null ? '' : this.state.price} required />
                </FormGroup>
                <FormGroup>
                    <Label for="description">Product Category</Label>
                    <select className="form-control" name="category_id" id="category_id" value={this.state.category_id} onChange={this.onChange}>
                        {this.state.category_arr.map((cat,i) => <option key={i} value={cat.value}>{cat.display}</option>)}
                    </select>
                </FormGroup>
                <Button>Submit</Button>
            </Form>
        );
    }
}

export default AddEditForm